package concurrencycoursework.display;

import java.awt.*;

// Since our applet has animations ( flags going up and down, status changing, etc ), we want to double buffer
// our graphics to avoid flickering.

public class DoubleBufferedCanvas extends Canvas {
    public void update(Graphics g) {
        // Graphics and Image to act as our buffer
        Graphics buffer;
        Image bufferedImage;

        // We only want the clip area
        Rectangle bounds = g.getClipBounds();

        // Create our buffer
        bufferedImage = createImage(bounds.width, bounds.height);
        buffer = bufferedImage.getGraphics();

        // Clear the canvas
        buffer.setColor(getBackground());
        buffer.fillRect(0, 0, bounds.width, bounds.height);
        buffer.setColor(getForeground());

        // Call the paint() method
        buffer.translate(-bounds.x, -bounds.y);
        paint(buffer);

        // Draw our buffered image
        g.drawImage(bufferedImage, bounds.x, bounds.y, this);
    }
}