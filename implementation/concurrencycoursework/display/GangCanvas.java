package concurrencycoursework.display;

import concurrencycoursework.resources.ResourceManager;

import java.awt.*;

public class GangCanvas extends DoubleBufferedCanvas {
    // Fonts
    Font f1 = new Font("Helvetica", Font.BOLD, 14);
    Font f2 = new Font("Times", Font.ITALIC + Font.BOLD, 22);

    // Member variables
    String gangName = null;
    String gangStatus = null;
    Image flagUpImage = null;
    Image flagDownImage = null;
    Image flagImage = null;
    boolean flagRaised = false;

    // Constructors
    public GangCanvas(String name) {
        this(name, "Idle", Color.cyan);
    }

    public GangCanvas(String name, String status) {
        this(name, status, Color.cyan);
    }

    public GangCanvas(String name, String status, Color color) {
        super();
        gangName = name;
        gangStatus = status;
        setBackground(color);

        // Set the flag images
        flagUpImage = ResourceManager.getResource("flagUp");
        flagDownImage = ResourceManager.getResource("flagDown");

        // The flag is initially lowered
        flagImage = flagDownImage;
    }

    // Setters
    public void setName(String name) {
        gangName = name;
        repaint();
    }

    public void setStatus(String status) {
        gangStatus = status;
        repaint();
    }

    public void setColor(Color color) {
        setBackground(color);
        repaint();
    }

    public void setFlagRaised(boolean raised) {
        flagRaised = raised;
        flagImage = (flagRaised) ? flagUpImage : flagDownImage;
        repaint();
    }

    // Paint method
    public void paint(Graphics g) {
        super.paint(g);

        // Draw flag image
        g.drawImage(flagImage, 50, 70, null);

        // Display the gang name
        g.setFont(f2);
        FontMetrics fm = g.getFontMetrics();
        int w = fm.stringWidth(gangName);
        int h = fm.getHeight();
        int x =(getSize().width - w)/2;
        int y = h;
        g.drawString(gangName, x, y);
        g.drawLine(x, y+3, x+w, y+3);

        // Display the status
        g.setFont(f1);
        fm = g.getFontMetrics();
        w = fm.stringWidth(gangStatus);
        h = fm.getHeight();
        x = (getSize().width - w)/2;
        y = h*3;
        g.drawString(gangStatus, x, y);
    }
}
