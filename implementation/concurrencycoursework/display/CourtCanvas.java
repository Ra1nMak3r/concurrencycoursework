package concurrencycoursework.display;

import concurrencycoursework.resources.ResourceManager;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class CourtCanvas extends DoubleBufferedCanvas {
    // Fonts
    Font f1 = new Font("Times", Font.BOLD, 20);

    // Member variables
    int occupiedBy;
    int turnIndicator;
    Image backgroundImage = null;

    // Constructors
    public CourtCanvas() {
        super();
        occupiedBy = 0;
        turnIndicator = 0;
        backgroundImage = ResourceManager.getResource("court");
    }

    // Setters
    public void setOccupiedBy(int gangNumber) {
        occupiedBy = gangNumber;
        repaint();
    }

    public void setTurnIndicator(int turn) {
        turnIndicator = turn;
        repaint();
    }

    public void setBackgroundImage(String imagePath) {
        backgroundImage = Toolkit.getDefaultToolkit().createImage(imagePath);
        repaint();
    }

    // Getters
    public int getTurnIndicator() {
        return turnIndicator;
    }

    // Paint method
    public void paint(Graphics g) {
        // Call parent's method
        super.paint(g);

        // Draw background image
        g.drawImage(backgroundImage, 0, 0, null);

        // Set font for the court status text
        g.setFont(f1);

        // Get the string's width and height and use them to determine its position
        String courtStatus = "";
        courtStatus += (occupiedBy == 0) ? "Unoccupied." : "Occupied by: " + occupiedBy;
        courtStatus += (turnIndicator == 0) ? " Turn: Not set. " : " Turn: " + turnIndicator;
        FontMetrics fm = g.getFontMetrics();
        int w = fm.stringWidth(courtStatus);
        int h = fm.getHeight();
        int x =(getSize().width - w)/2;
        int y = h;

        // Draw a white rectangle background behind the text
        Rectangle2D background = fm.getStringBounds(courtStatus, g);
        g.setColor(Color.white);
        g.fillRect(x-2, y - fm.getAscent(), (int)background.getWidth()+7, (int)background.getHeight());

        // Draw the court status text itself
        g.setColor(Color.black);
        g.drawString(courtStatus, x, y);
    }
}