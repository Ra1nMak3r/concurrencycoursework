package concurrencycoursework.resources;

import concurrencycoursework.helpers.ImageHelper;

import java.awt.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class ResourceManager {
    // a map to store all resources
    private static Map resources = new HashMap();

    // add a resource to the map
    public static void addResource(URL resourceURL, String resourceID) {
        resources.put(resourceID, ImageHelper.loadImageSynchronously(resourceURL));
    }

    // retrieve a resource from the map
    public static Image getResource(String resourceID) {
        if (resources.containsKey(resourceID)) {
            return (Image)resources.get(resourceID);
        }
        else {
            return null;
        }
    }

    // add transparency to image resource
    public static void makeTransparent(String resourceID) {
        if (resources.containsKey(resourceID))
            resources.put(resourceID, ImageHelper.addTransparency((Image)resources.get(resourceID), Color.white));
    }
}
