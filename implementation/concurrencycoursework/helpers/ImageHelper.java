package concurrencycoursework.helpers;

import java.awt.*;
import java.awt.image.*;
import java.net.URL;

public class ImageHelper extends Canvas {
    // A canvas to be passed to the media tracker and used for loading images
    private static Canvas imageLoadingCanvas = new Canvas();

    // Media Tracker to keep track of the images being loaded
    private static MediaTracker mediaTracker = new MediaTracker(imageLoadingCanvas);

    // next image ID in the media tracker
    private static int nextImageID = 0;

    // Load image synchronously from file path; calls createImage and waits until loading is finished
    public static Image loadImageSynchronously(String imagePath) {
        Image loadedImage = Toolkit.getDefaultToolkit().createImage(imagePath);
        waitForImage(loadedImage, nextImageID++);
        return loadedImage;
    }

    // Load image synchronously from image producer; calls createImage and waits until loading is finished
    public static Image loadImageSynchronously(ImageProducer imageProducer) {
        Image loadedImage = Toolkit.getDefaultToolkit().createImage(imageProducer);
        waitForImage(loadedImage, nextImageID++);
        return loadedImage;
    }

    // Load image synchronously from resource url; calls createImage and waits until loading is finished
    public static Image loadImageSynchronously(URL resourceURL) {
        Image loadedImage = Toolkit.getDefaultToolkit().createImage(resourceURL);
        waitForImage(loadedImage, nextImageID++);
        return loadedImage;
    }

    // Register image with the media tracker and wait until loading is finished. Used in loadImageSynchronously
    private static void waitForImage(Image image, int imageID) {
        // Register the image with the media tracker
        mediaTracker.addImage(image,imageID);

        // Check to see if the image has loaded and wait for if not
        if (!mediaTracker.checkID(imageID)) {
            try {
                mediaTracker.waitForID(imageID);
            }
            catch (Exception e) {
            }
        }
    }

    // Makes a certain color in the passed image transparent
    public static Image addTransparency(Image image, final Color color) {
        ImageFilter filter = new RGBImageFilter() {
            // Set alpha bits to opaque
            public int targetRGB = 0xFFFFFFFF;

            public final int filterRGB(final int x, final int y, final int rgb) {
                if ((rgb | 0xFF000000) == targetRGB) {
                    return 0x00FFFFFF & rgb;
                }
                else {
                    return rgb;
                }
            }
        };

        // Create image producer with filter
        final ImageProducer producer = new FilteredImageSource(image.getSource(), filter);

        // Load image using the producer and return it once the loading is finished
        Image transparentImage = loadImageSynchronously(producer);
        return transparentImage;
    }
}