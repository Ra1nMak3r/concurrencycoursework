package concurrencycoursework.court;

import concurrencycoursework.display.*;
import concurrencycoursework.resources.*;

import java.applet.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Court extends Applet implements ActionListener {
    // UI Elements
    Button startButton;
    CourtCanvas courtCanvas;
    GangCanvas sharksCanvas;
    GangCanvas jetsCanvas;

    // Monitor
    FlagSystem flags;

    // Threads
    Gang sharks;
    Gang jets;

    // Start button action
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == startButton) {
            flags = new FlagSystem(courtCanvas);
            sharks = new Gang(sharksCanvas, flags, 1, 11);
            jets = new Gang(jetsCanvas, flags, 2, 11);
            sharks.start();
            jets.start();
        }
        else {
            super.processEvent(event);
        }
    }

    // synchronously load the images used the applet and add them to the resource manager
    private void preLoadResources() {
        ResourceManager.addResource(getClass().getResource("/concurrencycoursework/resources/images/court.png"), "court");
        ResourceManager.addResource(getClass().getResource("/concurrencycoursework/resources/images/flagup.png"), "flagUp");
        ResourceManager.addResource(getClass().getResource("/concurrencycoursework/resources/images/flagdown.png"), "flagDown");

        // Make the white background of the flag images transparent
        ResourceManager.makeTransparent("flagUp");
        ResourceManager.makeTransparent("flagDown");
    }

    public void init() {
        super.init();

        // Load the resources for the canvases during applet initialization and before that actual canvases
        // are created, so that they are ready to use on canvas creation and there is no flickering or
        // images appearing one by one as they are loaded
        preLoadResources();

        // Setup and add the start button
        Panel panel0 = new Panel();
        panel0.add(startButton = new Button(" START "));
        startButton.setFont(new Font("Helvetica", Font.BOLD, 24));
        startButton.addActionListener(this);
        Panel panel1 = new Panel();
        panel1.setLayout(new BorderLayout());
        panel1.add("Center", panel0);

        // Set up display
        Panel panel2 = new Panel();
        courtCanvas = new CourtCanvas();
        sharksCanvas = new GangCanvas("SHARKS", "Idle", Color.yellow);
        jetsCanvas = new GangCanvas("JETS", "Idle", Color.green);

        // Set up canvas sizes
        courtCanvas.setSize(350, 250);
        sharksCanvas.setSize(200, 250);
        jetsCanvas.setSize(200, 250);

        // Add canvases
        panel2.add(sharksCanvas);
        panel2.add(courtCanvas);
        panel2.add(jetsCanvas);

        // Arrange Applet Display
        setLayout(new BorderLayout());
        add("Center", panel2);
        add("South", panel1);

        // Set applet size
        setSize(800,300);
    }
}
