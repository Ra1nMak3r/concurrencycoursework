package concurrencycoursework.court;

import concurrencycoursework.display.GangCanvas;

import java.util.Random;

/*
This is the thread class which maps to GANGS from 2b.lts
 */

public class Gang extends Thread {
    // The number of the gang -> 1 = Sharks; 2 = Jets
    int gangNumber;
    // The time in seconds for which the gang occupies the court; duration of one game
    int occupationTime;
    GangCanvas gangCanvas;
    FlagSystem flags;
    Random randomGenerator;

    Gang(GangCanvas canvas, FlagSystem flagSystem, int number, int occupation) {
        gangNumber = number;
        occupationTime = occupation;
        gangCanvas = canvas;
        flags = flagSystem;
        randomGenerator = new Random();
        randomGenerator.setSeed(gangNumber);
    }

    // Changes the status of the GangCanvas and sleeps for 2 seconds to make the change noticeable
    public void changeStatus(String status) {
        try {
            gangCanvas.setStatus(status);
            Thread.sleep(1800);
        }
        catch (InterruptedException e) {
        }
    }

    public void run() {
        try {
            for (int i=0; i<3; i++) {
                // Wait with "Idle" status
                Thread.sleep(gangNumber * 1800);

                changeStatus("Raising flag");

                // Raise flag
                // Maps to the "raiseFlag" action in GANGS
                gangCanvas.setFlagRaised(true);
                flags.raiseFlag();

                changeStatus("Changing Turn Indicator");

                // Set turn indicator
                // Maps to the "setTurnIndicator" action in GANGS
                flags.setTurnIndicator(gangNumber);

                changeStatus("Checking flag and turn");

                // Check the status of the flags and the turn indicator; this is where the wait will occur
                // Maps to the "checkFlagAndTurn" action in CHECKFLAGANDTURN
                flags.checkFlagAndTurn(gangNumber, gangCanvas);

                gangCanvas.setStatus("Using court");

                // Occupy the court for the duration of one game
                // Maps to the "use" action in WAITORUSE
                flags.useCourt(gangNumber);
                Thread.sleep(occupationTime * 1000);

                changeStatus("Lowering flag");

                // Leave and lower flag
                // Maps to the "lowerFlag" action in WAITORUSE
                flags.lowerFlag();
                gangCanvas.setFlagRaised(false);

                changeStatus("Idle");

                // Add some radnomness to the amount of time to wait until using the court again
                Thread.sleep(randomGenerator.nextInt(6)*1000 );
            }
        }
        catch (InterruptedException e) {
        }
    }
}