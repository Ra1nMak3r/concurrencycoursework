package concurrencycoursework.court;

import concurrencycoursework.display.CourtCanvas;
import concurrencycoursework.display.GangCanvas;

import java.awt.*;

/*
This is the monitor class which maps to FLAGANDINDICATORCONTROL from 2b.lts
 */

public class FlagSystem {
    // Number of gangs/flags - "const N" in the lts
    int maxFlags = 2;
    // Number of currently raised flags - the "flags" parameter of the processes in FLAGANDINDICATORCONTROL
    int flagsRaised = 0;

    CourtCanvas courtCanvas;

    FlagSystem(CourtCanvas canvas) {
        courtCanvas = canvas;
    }

    // raise the flag - maps to the raiseFlag action in FLAGSANDTURN
    synchronized void raiseFlag() {
        if (flagsRaised < maxFlags)
            flagsRaised++;
    }

    // lower the flag and leave the court - maps to the lowerFlag action in FLAGSANDTURN
    synchronized void lowerFlag() {
        if (flagsRaised > 0)
            flagsRaised--;
        courtCanvas.setOccupiedBy(0);
        notifyAll();
    }

    // set the turn indicator to the other gang's number - maps to the setTurnIndicator action in SETTURNINDICATOR
    synchronized void setTurnIndicator(int gangNumber) {
        if (gangNumber == 1)
            courtCanvas.setTurnIndicator(2);
        else
            courtCanvas.setTurnIndicator(1);
        notifyAll();
    }

    // check the status of the flag and turn indicator and wait if needed - maps to the checkFlagAndTurn action
    // in FLAGSANDTURN
    synchronized void checkFlagAndTurn(int gangNumber, GangCanvas gangCanvas) throws InterruptedException  {
        while (flagsRaised == 2 && courtCanvas.getTurnIndicator() != gangNumber) {
            gangCanvas.setStatus("Waiting");
            wait(); // maps to the wait action in CHECKFLAGANDTURN
        }
    }

    // occupy the court - maps to the use action in CHECKFLAGANDTURN
    synchronized void useCourt(int gangNumber)  {
        courtCanvas.setOccupiedBy(gangNumber);
    }
}
